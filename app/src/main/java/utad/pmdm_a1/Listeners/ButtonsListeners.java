package utad.pmdm_a1.Listeners;

import android.util.Log;
import android.view.View;
import android.widget.Toast;

import utad.pmdm_a1.MainActivity;
import utad.pmdm_a1.R;

/**
 * Created by Yony on 30/09/15.
 */
public class ButtonsListeners implements View.OnClickListener {

    MainActivity MA=null;
    String sLogVTAG="ButtonsListeners";

    public ButtonsListeners(MainActivity MAL){
        this.MA=MAL;
    }

    @Override
    public void onClick(View v) {
        if(v.getId()== R.id.button_ok){
            Toast.makeText(MA.getApplicationContext(),"PRESSED OK",Toast.LENGTH_LONG).show();
            Log.v(sLogVTAG, ""+MA.getEdtxt_nombre().getText());
            Log.v(sLogVTAG, "" + MA.getEdtxt_apellidos().getText());
            Log.v(sLogVTAG, ""+MA.getEdtxt_edad().getText());
            Log.v(sLogVTAG, ""+MA.getEdtxt_email().getText());
            Log.v(sLogVTAG, ""+MA.getEdtxt_password().getText());
        }
        if(v.getId()== R.id.button_cancel){
            Toast.makeText(MA.getApplicationContext(),"PRESSED CANCEL",Toast.LENGTH_LONG).show();
            MA.getEdtxt_nombre().setText("");
            MA.getEdtxt_apellidos().setText("");
            MA.getEdtxt_edad().setText("");
            MA.getEdtxt_email().setText("");
            MA.getEdtxt_password().setText("");
        }
    }
}
