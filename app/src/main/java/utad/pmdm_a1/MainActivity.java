package utad.pmdm_a1;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import utad.pmdm_a1.Listeners.ButtonsListeners;

public class MainActivity extends ActionBarActivity {

    EditText edtxt_nombre=null;
    EditText edtxt_apellidos=null;
    EditText edtxt_edad=null;
    EditText edtxt_email=null;
    EditText edtxt_password=null;

    Button btn_ok=null;
    Button btn_cancel=null;

    ButtonsListeners bl1=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initButtons();
        initEditTexts();
    }

    public void initButtons(){
        bl1=new ButtonsListeners(this);
        btn_ok=(Button)findViewById(R.id.button_ok);
        btn_ok.setOnClickListener(bl1);
        btn_cancel=(Button)findViewById(R.id.button_cancel);
        btn_cancel.setOnClickListener(bl1);
    }

    public void initEditTexts(){
        edtxt_nombre=(EditText)findViewById(R.id.editText_nombre);
        edtxt_apellidos=(EditText)findViewById(R.id.editText2_apellidos);
        edtxt_edad=(EditText)findViewById(R.id.editText3_edad);
        edtxt_email=(EditText)findViewById(R.id.editText4_email);
        edtxt_password=(EditText)findViewById(R.id.editText5_pass);
    }

    public EditText getEdtxt_nombre() {
        return edtxt_nombre;
    }

    public EditText getEdtxt_apellidos() {
        return edtxt_apellidos;
    }

    public EditText getEdtxt_edad() {
        return edtxt_edad;
    }

    public EditText getEdtxt_email() {
        return edtxt_email;
    }

    public EditText getEdtxt_password() {
        return edtxt_password;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
